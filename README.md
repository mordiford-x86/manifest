# mordiford/AICP-x86 (UNOFFICIAL)

![](https://lindwurm.neocities.org/img/mordiford_aicp-x86_mini.png)

AICP-n-12.1 (Android 7.1.1) for x86 PC. based on Android-x86 Project.

> Need more infomation? see our Project Wiki: https://gitlab.com/mordiford-x86/manifest/wikis/home

## How to Build

### Download the Source

Please read the [AOSP building instructions](http://source.android.com/source/index.html) before proceeding.

#### Initializing Repository

Initialize repo:

```
repo init -u https://gitlab.com/mordiford-x86/manifest.git -b n7.1-x86
```

sync repo :

```
repo sync -j8 -c -f --force-sync --no-clone-bundle
```

### Building

> **Note from https://groups.google.com/forum/#!topic/android-x86/J8pUu35VN5g :** At this moment you need to remove the cm kernel.mk and bt test_vendor_lib to get a complete build

```
rm -rf system/bt/vendor_libs/test_vendor_lib
```

Ok let's build

```
. build/envsetup.sh
```

```
breakfast aicp_android-x86-userdebug
```

```
m -j8 iso_img
```

## Credits

- Android-x86 http://www.android-x86.org/
- LineageOS http://lineageos.org/
- Android Ice Cold Project http://aicp-rom.com/
